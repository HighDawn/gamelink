package com.gamelinkdev.gamelink.ui.match_history

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class MatchHistoryViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MatchHistoryViewModel::class.java)) {
            return MatchHistoryViewModel() as T
        }
        throw IllegalArgumentException("Unknown viewModel class")
    }
}