package com.gamelinkdev.gamelink.ui.account

data class UpdateResult(
    val success: Boolean,
    val field: String
)