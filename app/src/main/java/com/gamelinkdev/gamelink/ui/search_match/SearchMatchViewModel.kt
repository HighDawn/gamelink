package com.gamelinkdev.gamelink.ui.search_match

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentValues.TAG
import android.content.Intent
import android.util.Log
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.data.model.Game
import com.gamelinkdev.gamelink.data.model.Match
import com.gamelinkdev.gamelink.data.model.Mode
import com.gamelinkdev.gamelink.data.model.User
import com.gamelinkdev.gamelink.ui.match_details.MatchDetailsActivity
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration

class SearchMatchViewModel : ViewModel() {
    private val _searchingMatch = MutableLiveData<Match>()
    val searchingMatch: LiveData<Match> = _searchingMatch

    private val _currentlySearching = MutableLiveData<Boolean>()
    val currentlySearching: LiveData<Boolean> = _currentlySearching

    private val _resumingSearch = MutableLiveData<Boolean>()
    val resumingSearch: LiveData<Boolean> = _resumingSearch

    private val db = FirebaseFirestore.getInstance()

    private var matchToListen: ListenerRegistration? = null
    private var matchId: String? = null

    init {
        _currentlySearching.value = false
        _resumingSearch.value = false
        _searchingMatch.value = Match()

        // Check with firestore if we already are in the middle of a match search
        db.collection("user_history").whereArrayContains("lobby", GlobalVars.currentUser?.id!!).whereEqualTo("open", true).get()
            .addOnSuccessListener { result ->
                if(result.size() > 0 ){
                    // User is already in a search, skip to that
                    matchId = result.documents[0].id

                    val matchToJoin = Match()
                    val data = result.documents[0].data!!

                    matchToJoin.open = data["open"] as Boolean
                    matchToJoin.game = Game.getGameById((data["game"] as Long).toInt())
                    matchToJoin.gameMode = Mode.getModeById((data["mode"] as Long).toInt())
                    matchToJoin.time = (data["date"] as Timestamp).toDate()

                    _searchingMatch.value = matchToJoin

                    _resumingSearch.value = true
                }
            }
    }

    fun setGame(game: Game) {
        _searchingMatch.value?.game = game
    }

    fun setGameMode(mode: Int, actualIndex: Boolean) {
        if (actualIndex) {
            _searchingMatch.value?.gameMode = Mode.getModeById(mode)
        }
        else {
            _searchingMatch.value?.gameMode = _searchingMatch.value?.game!!.modes[mode]
        }

        // Check if we're already in the middle of a search
        if (_resumingSearch.value!!) {
            resumeSearch()
        }
        else {
            startSearch()
        }
    }

    private fun startSearch() {
        _currentlySearching.value = true

        searchForMatch(_searchingMatch.value!!)
    }

    private fun resumeSearch() {
        db.collection("user_history").document(matchId!!).get()
            .addOnSuccessListener { result ->
                val matchToJoin = Match()
                val data = result.data!!

                matchToJoin.open = data["open"] as Boolean
                matchToJoin.game = Game.getGameById((data["game"] as Long).toInt())
                matchToJoin.gameMode = Mode.getModeById((data["mode"] as Long).toInt())
                matchToJoin.time = (data["date"] as Timestamp).toDate()

                (data["lobby"] as ArrayList<String>).forEach { user ->
                    matchToJoin.users.add(User(null, null, null, user, null))
                }

                _searchingMatch.value = matchToJoin
            }

        addMatchListener(matchId!!)
    }

    private fun searchForMatch(match: Match) {
        // Check if there are any matches open that correspond to our wanted criteria
        db.collection("user_history").whereEqualTo("game", match.game?.id).whereEqualTo("mode", match.gameMode?.id).whereEqualTo("open", true).get()
            .addOnSuccessListener { result ->
                if(result.size() > 0) {
                    val matchToJoin = Match()
                    val data = result.documents[0].data!!

                    matchToJoin.open = true
                    matchToJoin.game = match.game
                    matchToJoin.gameMode = match.gameMode
                    matchToJoin.time = (data["date"] as Timestamp).toDate()

                    (data["lobby"] as ArrayList<String>).forEach { user ->
                        matchToJoin.users.add(User(null, null, null, user, null))
                    }

                    matchToJoin.users.add(GlobalVars.currentUser!!)

                    _searchingMatch.value = matchToJoin
                    matchId = result.documents[0].id

                    db.collection("user_history").document(result.documents[0].id).update("lobby", matchToJoin.users.map { it.id })
                        .addOnSuccessListener {
                            Log.d(TAG, "User successfully added to match!")
                        }
                        .addOnFailureListener { exception ->
                            Log.e(TAG, "Error adding user to match", exception)
                        }

                    if(matchToJoin.users.size >= matchToJoin.gameMode?.maxLobby!!){
                        db.collection("user_history").document(result.documents[0].id).update("open", false)
                            .addOnSuccessListener {
                                Log.d(TAG, "Match successfully closed!")
                            }
                            .addOnFailureListener {exception ->
                                Log.e(TAG, "Failure closing match", exception)
                            }
                    }

                    addMatchListener(result.documents[0].id)
                }
                else {
                    createMatch(match)
                }
            }
    }

    private fun createMatch(match: Match) {
        val matchData = HashMap<String, Any>()

        matchData["game"] = match.game?.id!!
        matchData["mode"] = match.gameMode?.id!!
        matchData["date"] = Timestamp.now()
        matchData["open"] = true
        matchData["lobby"] = arrayListOf(GlobalVars.currentUser?.id)

        db.collection("user_history").add(matchData)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot writtent with ID: ${documentReference.id}")

                matchId = documentReference.id

                addMatchListener(documentReference.id)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error adding document", exception)
            }
    }

    private fun addMatchListener(documentId: String) {
        matchId = documentId

        // Add a listener for the match
        val docRef = db.collection("user_history").document(documentId)

        matchToListen = docRef.addSnapshotListener { snapshot, e ->
            if(e != null) {
                Log.w(TAG, "Listen failed", e)
                return@addSnapshotListener
            }

            if(snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: ${snapshot.data}")

                val matchToJoin = Match()
                val data = snapshot.data!!

                matchToJoin.open = data["open"] as Boolean
                matchToJoin.game = Game.getGameById((data["game"] as Long).toInt())
                matchToJoin.gameMode = Mode.getModeById((data["mode"] as Long).toInt())
                matchToJoin.time = (data["date"] as Timestamp).toDate()

                (data["lobby"] as ArrayList<String>).forEach { user ->
                    matchToJoin.users.add(User(null, null, null, user, null))
                }

                _searchingMatch.value = matchToJoin
            }
            else {
                Log.d(TAG, "Current data: null")
            }
        }
    }

    fun stopSearching() {
        _currentlySearching.value = false
        _resumingSearch.value = false

        matchToListen?.remove()

        // Check if we're the only one in the match
        if(_searchingMatch.value?.users?.size == 1) {
            // Delete the match from the database
            db.collection("user_history").document(matchId!!).delete()
        }
        else {
            // Delete our user from the database
            for(i in 0 until _searchingMatch.value?.users?.size!!) {
                if(_searchingMatch.value!!.users[i].id == GlobalVars.currentUser?.id) {
                    _searchingMatch.value?.users?.removeAt(i)
                    break
                }
            }

            db.collection("user_history").document(matchId!!).update("lobby", _searchingMatch.value?.users?.map { it.id })
        }
    }
}