package com.gamelinkdev.gamelink.ui.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentValues.TAG
import android.util.Log
import android.util.Patterns

import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.data.model.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class LoginViewModel : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    private val _authMethod = MutableLiveData<AuthMethod>()
    val authMethod: LiveData<AuthMethod> = _authMethod

    private val _passwordResetEmailSent = MutableLiveData<Boolean>()
    val passwordResetEmailSent: LiveData<Boolean> = _passwordResetEmailSent

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()

    init {
        _authMethod.value = AuthMethod(login = true, register = false)
    }

    fun changeAuthMethod() {
        if(_authMethod.value?.login!!){
            _authMethod.value = AuthMethod(login = false, register = true)
        }
        else{
            _authMethod.value = AuthMethod(login = true, register = false)
        }
    }

    fun login(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(OnCompleteListener<AuthResult> { task ->
            if(task.isSuccessful) {
                try {
                    getUserData()
                }
                catch (e: NullPointerException) {
                    _loginResult.value = LoginResult(error = R.string.login_failed)
                }
            }
            else {
                _loginResult.value = LoginResult(error = R.string.login_failed)
            }
        })
    }

    fun register(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(OnCompleteListener<AuthResult> { task ->
            if (task.isSuccessful) {
                try {
                    createUserData()
                }
                catch(e: NullPointerException) {
                    _loginResult.value = LoginResult(error = R.string.registration_failed)
                }
            }
            else {
                _loginResult.value = LoginResult(error = R.string.registration_failed)
            }
        })
    }

    fun sendResetPassword(email: String) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            _passwordResetEmailSent.value = task.isSuccessful
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    private fun createUserData() {
        val userID = auth.currentUser?.uid!!

        val userData = User()
        GlobalVars.currentUser = userData
        userData.id = userID
        GlobalVars.currentUser?.id = userID

        val docRef = db.collection("user_info").document(userID)
        docRef.set(userData).addOnSuccessListener {
                Log.d(TAG, "User data successfully written to Firebase!")
                _loginResult.value = LoginResult(success = true)
            }
            .addOnFailureListener { exception ->
                _loginResult.value = LoginResult(error = R.string.error_fetching_user_data)
                Log.e(TAG, exception.message)
            }
    }

    private fun getUserData() {
        val userID = auth.currentUser?.uid!!

        val docRef = db.collection("user_info").document(userID)
        docRef.get().addOnSuccessListener { document ->
                if (document != null) {
                    Log.d(TAG, "DocumentSnapshot id: ${document.id} data: ${document.data}")
                    GlobalVars.currentUser = document.toObject(User::class.java)
                    GlobalVars.currentUser?.id = userID
                    _loginResult.value = LoginResult(success = true)
                } else {
                    _loginResult.value = LoginResult(error = R.string.login_failed)
                }
            }
            .addOnFailureListener { exception ->
                _loginResult.value = LoginResult(error = R.string.error_fetching_user_data)
                Log.e(TAG, exception.message)
            }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}
