package com.gamelinkdev.gamelink.ui.account

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.support.v4.widget.DrawerLayout
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.ui.login.LoginActivity
import com.gamelinkdev.gamelink.ui.match_history.MatchHistoryActivity
import com.gamelinkdev.gamelink.ui.search_match.SearchMatchActivity
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nav_header_drawer.*

class AccountActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var accountViewModel: AccountViewModel

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_drawer)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        val navUsername = headerView.findViewById<TextView>(R.id.navbar_user_name)
        val navEmail = headerView.findViewById<TextView>(R.id.navbar_user_email)
        navUsername.text = GlobalVars.currentUser?.name
        navEmail.text = auth.currentUser?.email

        val avatar = findViewById<ImageView>(R.id.userAvatarImageView)
        val username = findViewById<EditText>(R.id.userNameEditText)
        val description = findViewById<EditText>(R.id.userDescriptionEditText)
        val epicAccount = findViewById<EditText>(R.id.userAccountEpicEditText)
        val steamAccount = findViewById<EditText>(R.id.userAccountSteamEditText)
        val riotAccount = findViewById<EditText>(R.id.userAccountRiotEditText)

        accountViewModel = ViewModelProviders.of(this, AccountViewModelFactory())
            .get(AccountViewModel::class.java)

        accountViewModel.updateResult.observe(this@AccountActivity, Observer {
            val updateState = it ?: return@Observer

            var toastMessage: String

            if (updateState.success) {
                toastMessage = "Changed " + updateState.field + " successfully!"
            }
            else {
                toastMessage = "Error updating " + updateState.field
            }

            Toast.makeText(
                applicationContext,
                toastMessage,
                Toast.LENGTH_LONG
            ).show()
        })

        accountViewModel.userData.observe(this@AccountActivity, Observer {
            val userData = it ?: return@Observer

            // Check if user has an avatar
            if(userData.avatar == null) {
                Picasso.get().load(R.drawable.ic_menu_account_circle).error(R.drawable.ic_menu_account_circle).resize(400, 400).centerCrop().into(avatar)
            }
            else {
                Picasso.get().load(userData.avatar).resize(400, 400).centerCrop().into(avatar)
            }

            username.setText(userData.name)
            description.setText(userData.description)

            epicAccount.setText(userData.accounts?.epic)
            steamAccount.setText(userData.accounts?.steam)
            riotAccount.setText(userData.accounts?.riot)
        })

        username.setOnEditorActionListener { v, actionId, event ->
            accountViewModel.updateUserData("name", v.text.toString())

            true
        }

        description.setOnEditorActionListener { v, actionId, event ->
            accountViewModel.updateUserData("description", v.text.toString())

            true
        }

        epicAccount.setOnEditorActionListener { v, actionId, event ->
            accountViewModel.updateUserData("accounts.epic", v.text.toString())

            true
        }

        steamAccount.setOnEditorActionListener { v, actionId, event ->
            accountViewModel.updateUserData("accounts.steam", v.text.toString())

            true
        }

        riotAccount.setOnEditorActionListener { v, actionId, event ->
            accountViewModel.updateUserData("accounts.riot", v.text.toString())

            true
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.search_match -> {
                val intent = Intent(this, SearchMatchActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.match_history -> {
                val intent = Intent(this, MatchHistoryActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.logout -> {
                auth.signOut()
                val intent = Intent(this, LoginActivity::class.java)
                finish()
                startActivity(intent)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
