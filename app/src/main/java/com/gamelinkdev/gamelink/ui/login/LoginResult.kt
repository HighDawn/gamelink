package com.gamelinkdev.gamelink.ui.login

import com.gamelinkdev.gamelink.data.model.User

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
    val success: Boolean = false,
    val error: Int? = null
) {
    constructor() : this(false, null)
}
