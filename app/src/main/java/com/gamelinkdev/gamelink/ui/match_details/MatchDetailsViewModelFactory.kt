package com.gamelinkdev.gamelink.ui.match_details

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class MatchDetailsViewModelFactory : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MatchDetailsViewModel::class.java)) {
            return MatchDetailsViewModel() as T
        }
        throw IllegalArgumentException("Unknown viewModel class")
    }
}