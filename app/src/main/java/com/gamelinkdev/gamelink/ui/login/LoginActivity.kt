package com.gamelinkdev.gamelink.ui.login

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*

import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.ui.account.AccountActivity
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        val email = findViewById<EditText>(R.id.email)
        val password = findViewById<EditText>(R.id.password)
        val confirmPassword = findViewById<EditText>(R.id.password_confirm)
        val loginButton = findViewById<Button>(R.id.login)
        val changeAuthMethodButton = findViewById<Button>(R.id.change_auth_method)
        val forgotPasswordButton = findViewById<Button>(R.id.forgot_password)
        val loading = findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            loginButton.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                email.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success) {
                openMainActivity()

                setResult(Activity.RESULT_OK)

                //Complete and destroy login activity once successful
                finish()
            }
        })

        loginViewModel.authMethod.observe(this@LoginActivity, Observer {
            val authMethod = it ?: return@Observer

            if(authMethod.login) {
                changeAuthMethodButton.text = getString(R.string.action_sign_up)
                loginButton.text = getString(R.string.action_sign_in)
                confirmPassword.visibility = View.GONE
            }
            else{
                changeAuthMethodButton.text = getString(R.string.action_sign_in)
                loginButton.text = getString(R.string.action_sign_up)
                confirmPassword.visibility = View.VISIBLE
            }
        })

        loginViewModel.passwordResetEmailSent.observe(this@LoginActivity, Observer {
            val passwordResetEmailSent = it ?: return@Observer

            if(passwordResetEmailSent) {
                Toast.makeText(
                    applicationContext,
                    "Password reset email sent!",
                    Toast.LENGTH_LONG
                ).show()
            }
            else {
                Toast.makeText(
                    applicationContext,
                    "Error sending reset password email",
                    Toast.LENGTH_LONG
                ).show()
            }
        })

        email.afterTextChanged {
            loginViewModel.loginDataChanged(
                email.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    email.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            email.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            loginButton.setOnClickListener {
                loading.visibility = View.VISIBLE

                // Check whether we're logging in or registering
                if(loginViewModel.authMethod.value?.login!!) {
                    loginViewModel.login(email.text.toString(), password.text.toString())
                }
                else{
                    loginViewModel.register(email.text.toString(), password.text.toString())
                }
            }

            changeAuthMethodButton.setOnClickListener {
                loginViewModel.changeAuthMethod()
            }

            forgotPasswordButton.setOnClickListener {
                if(!email.text.isNullOrEmpty()) {
                    loginViewModel.sendResetPassword(email.text.toString())
                }
                else {
                    Toast.makeText(
                        applicationContext,
                        "Please introduce an email",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        auth = FirebaseAuth.getInstance()
    }

    private fun openMainActivity() {
        val intent = Intent(this, AccountActivity::class.java)

        startActivity(intent)
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}
