package com.gamelinkdev.gamelink.ui.match_details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.model.Game
import com.gamelinkdev.gamelink.data.model.Match
import com.gamelinkdev.gamelink.data.model.Mode
import com.gamelinkdev.gamelink.data.model.User
import com.gamelinkdev.gamelink.ui.match_history.MatchHistoryActivity
import kotlinx.android.synthetic.main.activity_match_details.*
import java.util.*
import kotlin.collections.ArrayList

class MatchDetailsActivity : AppCompatActivity() {
    private lateinit var matchDetailsViewModel: MatchDetailsViewModel
    private lateinit var matchDetailsAdapter: MatchDetailsRecyclerViewAdapter

    var currentMatch: Match = Match()
    var usersInMatch: ArrayList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_details)

        matchDetailsViewModel = ViewModelProviders.of(this, MatchDetailsViewModelFactory())
            .get(MatchDetailsViewModel::class.java)

        currentMatch = intent.extras.getSerializable("Match") as Match

        usersInMatch = currentMatch.users

        match_users_recyclerview.layoutManager = LinearLayoutManager(this)
        matchDetailsAdapter = MatchDetailsRecyclerViewAdapter(this, currentMatch.users, currentMatch.game?.platform!!)
        match_users_recyclerview.adapter = matchDetailsAdapter

        matchDetailsAdapter.updateDataset(usersInMatch)

        matchDetailsViewModel.setMatch(currentMatch)

        goBackButton.setOnClickListener {
            val intent = Intent(this, MatchHistoryActivity::class.java)
            finish()
            startActivity(intent)
        }

        matchDetailsViewModel.match.observe(this@MatchDetailsActivity, Observer {
            currentMatch = it ?: return@Observer

            gameName.text = (currentMatch.game as Game).title
            modeName.text = (currentMatch.gameMode as Mode).title
            matchTime.text = (currentMatch.time as Date).toString()

            matchDetailsAdapter.updateDataset(currentMatch.users)
        })
    }
}
