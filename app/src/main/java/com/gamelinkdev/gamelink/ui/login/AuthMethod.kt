package com.gamelinkdev.gamelink.ui.login

data class AuthMethod(
    val login: Boolean = true,
    val register: Boolean = false
)