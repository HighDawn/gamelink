package com.gamelinkdev.gamelink.ui.match_history

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentValues.TAG
import android.util.Log
import com.gamelinkdev.gamelink.data.model.Game
import com.gamelinkdev.gamelink.data.model.Match
import com.gamelinkdev.gamelink.data.model.Mode
import com.gamelinkdev.gamelink.data.model.User
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlin.collections.ArrayList

class MatchHistoryViewModel : ViewModel() {
    private val _matchesList = MutableLiveData<ArrayList<Match>>()
    val matchesList: LiveData<ArrayList<Match>> = _matchesList

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()

    init {
        getMatchesList()
    }

    private fun getMatchesList() {
        db.collection("user_history").whereArrayContains("lobby", auth.currentUser?.uid!!).get()
            .addOnSuccessListener { result ->
                var match: Match
                var data: Map<String, Any>
                var matchList: ArrayList<Match> = ArrayList()
                for(document in result) {
                    match = Match()
                    data = document.data
                    match.time = (data["date"] as Timestamp).toDate()
                    match.game = Game.getGameById((data["game"] as Long).toInt())
                    match.gameMode = Mode.getModeById((data["mode"] as Long).toInt())

                    (data["lobby"] as ArrayList<String>).forEach { user ->
                        match.users.add(User(null, null, null, user, null))
                    }

                    Log.d(TAG, "Got the following match: $match")

                    matchList.add(match)
                }

                _matchesList.value = matchList
            }
            .addOnFailureListener { exception ->
                Log.e(TAG, "Error getting matches.", exception)
            }
    }
}