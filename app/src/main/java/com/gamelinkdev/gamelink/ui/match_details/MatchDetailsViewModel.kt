package com.gamelinkdev.gamelink.ui.match_details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentValues.TAG
import android.util.Log
import com.gamelinkdev.gamelink.data.model.Game
import com.gamelinkdev.gamelink.data.model.Match
import com.gamelinkdev.gamelink.data.model.PlatformAccount
import com.gamelinkdev.gamelink.data.model.User
import com.google.firebase.firestore.FirebaseFirestore

class MatchDetailsViewModel : ViewModel() {
    private val _match = MutableLiveData<Match>()
    val match: LiveData<Match> = _match

    private val db = FirebaseFirestore.getInstance()

    fun setMatch(newMatch: Match) {
        // Cycle through all users in match and get their info
        for(i in 0 until newMatch.users.size) {
            db.collection("user_info").document(newMatch.users[i].id.toString()).get()
                .addOnSuccessListener { result ->
                    Log.d(TAG, "Got the user ${result.data}")

                    newMatch.users[i].id = newMatch.users[i].id
                    newMatch.users[i].name = result["name"] as String?
                    newMatch.users[i].description = result["description"] as String?
                    newMatch.users[i].avatar = result["avatar"] as String?
                    val accounts: HashMap<String, String> = result["accounts"] as HashMap<String, String>
                    newMatch.users[i].accounts = PlatformAccount(accounts["steam"], accounts["epic"], accounts["riot"])

                    _match.value = newMatch
                }
                .addOnFailureListener { exception ->
                    Log.e(TAG, "Failed to retrieve user with id: ${newMatch.users[i].id}")
                }
        }
    }
}