package com.gamelinkdev.gamelink.ui.search_match

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.data.model.Game
import com.gamelinkdev.gamelink.data.model.Platform
import com.gamelinkdev.gamelink.ui.account.AccountActivity
import com.gamelinkdev.gamelink.ui.login.LoginActivity
import com.gamelinkdev.gamelink.ui.match_details.MatchDetailsActivity
import com.gamelinkdev.gamelink.ui.match_history.MatchHistoryActivity
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_search_match.*

class SearchMatchActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var searchMatchViewModel: SearchMatchViewModel

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_match_drawer)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        val navUsername = headerView.findViewById<TextView>(R.id.navbar_user_name)
        val navEmail = headerView.findViewById<TextView>(R.id.navbar_user_email)
        navUsername.text = GlobalVars.currentUser?.name
        navEmail.text = auth.currentUser?.email

        searchMatchViewModel = ViewModelProviders.of(this, SearchMatchViewModelFactory())
            .get(SearchMatchViewModel::class.java)

        gameModeChooserLayout.visibility = View.GONE
        searchingMatchLayout.visibility = View.GONE

        csgoButton.setOnClickListener {
            if (checkHasAccount(Game.CSGO)) {
                selectGame(Game.CSGO)
            }
        }

        fortniteButton.setOnClickListener {
            if(checkHasAccount(Game.FORTNITE)) {
                selectGame(Game.FORTNITE)
            }
        }

        lolButton.setOnClickListener {
            if(checkHasAccount(Game.LEAGUEOFLEGENDS)) {
                selectGame(Game.LEAGUEOFLEGENDS)
            }
        }

        mode1Button.setOnClickListener {
            selectGameMode(0, false)
        }

        mode2Button.setOnClickListener {
            selectGameMode(1, false)
        }

        mode3Button.setOnClickListener {
            selectGameMode(2, false)
        }

        mode4Button.setOnClickListener {
            selectGameMode(3, false)
        }

        goBackToGameButton.setOnClickListener {
            backToGameSelection()
        }

        stopSearchingButton.setOnClickListener {
            stopSearching()
        }

        searchMatchViewModel.searchingMatch.observe(this@SearchMatchActivity, Observer {
            val matchToJoin = it ?: return@Observer

            if(searchMatchViewModel.currentlySearching.value!! || searchMatchViewModel.resumingSearch.value!!) {
                // Check if the match is full
                if(matchToJoin.users.size >= matchToJoin.gameMode?.maxLobby!!) {
                    val intent = Intent(this@SearchMatchActivity, MatchDetailsActivity::class.java)
                    intent.putExtra("Match", matchToJoin)
                    finish()
                    startActivity(intent)
                }

                Picasso.get().load(matchToJoin.game?.avatar).resize(400, 400).centerCrop().into(gameAvatar)
                searchingGameNameTextView.text = matchToJoin.game?.title
                searchingGameModeTextView.text = matchToJoin.gameMode?.title
                playersFoundEditText.text = "Players found: ${matchToJoin.users.size}/${matchToJoin.gameMode?.maxLobby}"
            }
        })

        searchMatchViewModel.resumingSearch.observe(this@SearchMatchActivity, Observer {
            val searchStatus = it ?: return@Observer

            if(searchStatus) {
                selectGame(searchMatchViewModel.searchingMatch.value?.game!!)
                selectGameMode(searchMatchViewModel.searchingMatch.value?.gameMode?.id!!, true)
            }
        })
    }

    private fun selectGame(game: Game) {
        searchMatchViewModel.setGame(game)

        gameChooserLayout.visibility = View.GONE
        gameModeChooserLayout.visibility = View.VISIBLE

        if(game.modes.size == 1) {
            mode1Button.text = game.modes[0].title
            mode1Button.visibility = View.VISIBLE
            mode2Button.visibility = View.GONE
            mode3Button.visibility = View.GONE
            mode4Button.visibility = View.GONE
        }
        else if(game.modes.size == 2) {
            mode1Button.text = game.modes[0].title
            mode2Button.text = game.modes[1].title
            mode1Button.visibility = View.VISIBLE
            mode2Button.visibility = View.VISIBLE
            mode3Button.visibility = View.GONE
            mode4Button.visibility = View.GONE
        }
        else if(game.modes.size == 3) {
            mode1Button.text = game.modes[0].title
            mode2Button.text = game.modes[1].title
            mode3Button.text = game.modes[2].title
            mode1Button.visibility = View.VISIBLE
            mode2Button.visibility = View.VISIBLE
            mode3Button.visibility = View.VISIBLE
            mode4Button.visibility = View.GONE
        }
        else if(game.modes.size == 4) {
            mode1Button.text = game.modes[0].title
            mode2Button.text = game.modes[1].title
            mode3Button.text = game.modes[2].title
            mode4Button.text = game.modes[3].title
            mode1Button.visibility = View.VISIBLE
            mode2Button.visibility = View.VISIBLE
            mode3Button.visibility = View.VISIBLE
            mode4Button.visibility = View.VISIBLE
        }
    }

    private fun selectGameMode(mode: Int, actualIndex: Boolean) {
        searchMatchViewModel.setGameMode(mode, actualIndex)

        gameModeChooserLayout.visibility = View.GONE
        searchingMatchLayout.visibility = View.VISIBLE
    }

    private fun backToGameSelection() {
        gameChooserLayout.visibility = View.VISIBLE
        gameModeChooserLayout.visibility = View.GONE
    }

    private fun stopSearching() {
        gameChooserLayout.visibility = View.VISIBLE
        gameModeChooserLayout.visibility = View.GONE
        searchingMatchLayout.visibility = View.GONE

        searchMatchViewModel.stopSearching()
    }

    private fun checkHasAccount(game: Game): Boolean {
        var hasAccount = false

        // Check if the user has the game platform associated to his profile
        when(game.platform) {
            Platform.STEAM -> if (GlobalVars.currentUser?.accounts?.steam != null && GlobalVars.currentUser?.accounts?.steam != ""){
                hasAccount = true
            }
            Platform.EPIC -> if (GlobalVars.currentUser?.accounts?.epic != null && GlobalVars.currentUser?.accounts?.epic != ""){
                hasAccount = true
            }
            Platform.RIOT -> if (GlobalVars.currentUser?.accounts?.riot != null && GlobalVars.currentUser?.accounts?.riot != ""){
                hasAccount = true
            }
        }

        if (hasAccount) {
            return true
        }

        AlertDialog.Builder(this)
            .setTitle("Error")
            .setMessage("You need to associate an ${game.platform.title} account to search for this game")
            .show()

        return false
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.account -> {
                val intent = Intent(this, AccountActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.match_history -> {
                val intent = Intent(this, MatchHistoryActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.logout -> {
                auth.signOut()
                val intent = Intent(this, LoginActivity::class.java)
                finish()
                startActivity(intent)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
