package com.gamelinkdev.gamelink.ui.match_details

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.model.Platform
import com.gamelinkdev.gamelink.data.model.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.match_details_item.view.*
import kotlinx.android.synthetic.main.match_history_item.view.*

class MatchDetailsRecyclerViewAdapter(private var context: Context, private var dataList: ArrayList<User>, private var platform: Platform): RecyclerView.Adapter<MatchDetailsRecyclerViewAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = dataList[position]
        holder.userName.text = user.name ?: "Loading..."

        when(platform) {
            Platform.STEAM -> holder.userAccount.text = user.accounts?.steam ?: "Loading..."
            Platform.EPIC -> holder.userAccount.text = user.accounts?.epic ?: "Loading..."
            Platform.RIOT -> holder.userAccount.text = user.accounts?.riot ?: "Loading..."
        }

        if(user.avatar == null) {
            Picasso.get().load(R.drawable.ic_menu_account_circle).error(R.drawable.ic_menu_account_circle).resize(200, 200).centerCrop().into(holder.userAvatar)
        }
        else {
            Picasso.get().load(user.avatar).resize(200, 200).centerCrop().into(holder.userAvatar)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.match_details_item, parent, false))
    }

    fun updateDataset(newData: ArrayList<User>) {
        dataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var userName: TextView = itemView.userName
        var userAvatar: ImageView = itemView.userAvatar
        var userAccount: TextView = itemView.userGameId
    }
}