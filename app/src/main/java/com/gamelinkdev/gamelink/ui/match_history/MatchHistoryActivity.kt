package com.gamelinkdev.gamelink.ui.match_history

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.data.model.*
import com.gamelinkdev.gamelink.ui.account.AccountActivity
import com.gamelinkdev.gamelink.ui.login.LoginActivity
import com.gamelinkdev.gamelink.ui.match_details.MatchDetailsActivity
import com.gamelinkdev.gamelink.ui.search_match.SearchMatchActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_match_history.*
import kotlin.collections.ArrayList

class MatchHistoryActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var matchHistoryViewModel: MatchHistoryViewModel
    private lateinit var matchesAdapter: MatchHistoryRecyclerViewAdapter

    private var matchesList: ArrayList<Match> = ArrayList()

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_history_drawer)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val headerView = navView.getHeaderView(0)
        val navUsername = headerView.findViewById<TextView>(R.id.navbar_user_name)
        val navEmail = headerView.findViewById<TextView>(R.id.navbar_user_email)
        navUsername.text = GlobalVars.currentUser?.name
        navEmail.text = auth.currentUser?.email

        matchHistoryViewModel = ViewModelProviders.of(this, MatchHistoryViewModelFactory())
            .get(MatchHistoryViewModel::class.java)

        matches_recyclerview.layoutManager = LinearLayoutManager(this)
        matchesAdapter = MatchHistoryRecyclerViewAdapter(this, matchesList)
        matches_recyclerview.adapter = matchesAdapter

        matchHistoryViewModel.matchesList.observe(this@MatchHistoryActivity, Observer {
            matchesList = it ?: return@Observer

            matchesAdapter.updateDataset(matchesList)
        })

        matchesAdapter.onItemClick = { match ->
            val intent = Intent(this@MatchHistoryActivity, MatchDetailsActivity::class.java)
            intent.putExtra("Match", match)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.account -> {
                val intent = Intent(this, AccountActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.search_match -> {
                val intent = Intent(this, SearchMatchActivity::class.java)
                finish()
                startActivity(intent)
            }
            R.id.logout -> {
                auth.signOut()
                val intent = Intent(this, LoginActivity::class.java)
                finish()
                startActivity(intent)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
