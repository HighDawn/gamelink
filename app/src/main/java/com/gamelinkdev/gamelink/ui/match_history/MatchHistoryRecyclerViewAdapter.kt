package com.gamelinkdev.gamelink.ui.match_history

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.gamelinkdev.gamelink.R
import com.gamelinkdev.gamelink.data.model.Match
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.match_history_item.view.*
import kotlin.collections.ArrayList

class MatchHistoryRecyclerViewAdapter(private var context: Context, private var dataList: ArrayList<Match>): RecyclerView.Adapter<MatchHistoryRecyclerViewAdapter.ViewHolder>() {
    var onItemClick: ((Match) -> Unit)? = null

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val match = dataList[position]
        holder.gameName.text = match.game?.title
        Picasso.get().load(match.game?.avatar).resize(200,200).centerCrop().into(holder.gameAvatar)
        holder.modeTitle.text = match.gameMode?.title
        holder.matchDate.text = match.time?.toString()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.match_history_item, parent, false))
    }

    fun updateDataset(newData: ArrayList<Match>) {
        dataList = newData
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(dataList[adapterPosition])
            }
        }

        var gameName: TextView = itemView.gameName
        var gameAvatar: ImageView = itemView.gameAvatar
        var modeTitle: TextView = itemView.modeName
        var matchDate: TextView = itemView.matchTime
    }
}