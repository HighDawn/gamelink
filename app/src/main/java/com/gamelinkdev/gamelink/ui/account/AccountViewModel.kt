package com.gamelinkdev.gamelink.ui.account

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentValues.TAG
import android.util.Log
import com.gamelinkdev.gamelink.data.GlobalVars
import com.gamelinkdev.gamelink.data.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class AccountViewModel : ViewModel() {
    private val _userData = MutableLiveData<User>()
    val userData: LiveData<User> = _userData

    private val _updateResult = MutableLiveData<UpdateResult>()
    val updateResult: LiveData<UpdateResult> = _updateResult

    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()

    init {
        _userData.value = GlobalVars.currentUser
    }

    fun updateUserData(fieldToUpdate: String, newValue: String) {
        val userID = auth.currentUser?.uid!!

        // Check if field to update is an account
        val fieldToSend = when(fieldToUpdate) {
            "accounts.epic" -> "Epic Account"
            "accounts.steam" -> "Steam Account"
            "accounts.riot" -> "Riot Account"
            else -> fieldToUpdate
        }

        // Update user with new field value
        when(fieldToUpdate) {
            "accounts.epic" -> GlobalVars.currentUser?.accounts?.epic = newValue
            "accounts.steam" -> GlobalVars.currentUser?.accounts?.steam = newValue
            "accounts.riot" -> GlobalVars.currentUser?.accounts?.riot = newValue
            "avatar" -> GlobalVars.currentUser?.avatar = newValue
            "description" -> GlobalVars.currentUser?.description = newValue
            "name" -> GlobalVars.currentUser?.name = newValue
        }

        val docRef = db.collection("user_info").document(userID)
        docRef.update(fieldToUpdate, newValue).addOnSuccessListener {
            _updateResult.value = UpdateResult(true, fieldToSend)
        }
        .addOnFailureListener { exception ->
            Log.e(TAG, exception.message)

            _updateResult.value = UpdateResult(false, fieldToSend)
        }
    }
}