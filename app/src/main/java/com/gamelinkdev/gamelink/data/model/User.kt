package com.gamelinkdev.gamelink.data.model

import java.io.Serializable

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class User(
    var name: String? = null,
    var description: String? = null,
    var avatar: String? = null,
    var id: String? = null,
    var accounts: PlatformAccount?
) : Serializable {
    constructor() : this("", null, null, null, PlatformAccount())
}

data class PlatformAccount(
    var steam: String? = null,
    var epic: String? = null,
    var riot: String? = null
) : Serializable {
    constructor() : this(null, null, null)
}
