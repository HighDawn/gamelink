package com.gamelinkdev.gamelink.data.model

import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class Match (
    var game: Game? = null,
    var gameMode: Mode? = null,
    var time: Date? = null,
    var users: ArrayList<User>,
    var open: Boolean
) : Serializable {
    constructor(): this(null, null, null, ArrayList(), true)
}