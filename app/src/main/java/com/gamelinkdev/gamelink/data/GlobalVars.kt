package com.gamelinkdev.gamelink.data

import android.app.Application
import com.gamelinkdev.gamelink.data.model.User

object GlobalVars : Application() {
    var currentUser : User? = User()
}