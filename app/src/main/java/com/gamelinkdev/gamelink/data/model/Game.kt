package com.gamelinkdev.gamelink.data.model

enum class Game (
    val title: String,
    val avatar: String? = null,
    val id: Int,
    val modes: ArrayList<Mode>,
    val platform: Platform
) {
    FORTNITE("Fortnite", "https://firebasestorage.googleapis.com/v0/b/gamelink-48be9.appspot.com/o/user_avatars%2F37740339_696681337381640_307365932924141568_n.png?alt=media&token=1cdc1898-ef04-4630-81d8-b3dd4cddba2d", 0, arrayListOf(Mode.RANKED_SQUAD, Mode.RANKED_DUO, Mode.CASUAL_SQUAD, Mode.CASUAL_DUO), Platform.EPIC),
    CSGO("Counter Strike: GO", "https://firebasestorage.googleapis.com/v0/b/gamelink-48be9.appspot.com/o/user_avatars%2FGrupo%20de%20m%C3%A1scara%201.png?alt=media&token=e925eecd-cf3d-4ebb-b96b-2242d1f9139b", 1, arrayListOf(Mode.CASUAL, Mode.COMPETITIVE), Platform.STEAM),
    LEAGUEOFLEGENDS("League of Legends", "https://firebasestorage.googleapis.com/v0/b/gamelink-48be9.appspot.com/o/user_avatars%2FGrupo%20de%20m%C3%A1scara%202.png?alt=media&token=548714a5-008b-45d5-91e1-44a58ab6006b",2, arrayListOf(Mode.CASUAL, Mode.COMPETITIVE, Mode.TWISTED, Mode.ARAM), Platform.RIOT);

    companion object {
        fun getGameById(id: Int): Game? {
            values().forEach { game ->
                if (game.id == id) {
                    return game
                }
            }

            return null
        }
    }
}

enum class Mode (
    val title: String,
    val id: Int,
    val maxLobby: Int
) {
    RANKED_SQUAD("Ranked Squad", 0, 4),
    RANKED_DUO("Ranked Duo", 1, 2),
    CASUAL_SQUAD("Casual Squad", 2, 4),
    CASUAL_DUO("Casual Duo", 3, 2),
    CASUAL("Casual", 4, 10),
    COMPETITIVE("Competitive", 5, 5),
    TWISTED("Twisted Treeline", 6, 5),
    ARAM("ARAM", 7, 5);

    companion object {
        fun getModeById(id: Int): Mode? {
            values().forEach { mode ->
                if (mode.id == id) {
                    return mode
                }
            }

            return null
        }
    }
}

enum class Platform (
    val title: String
) {
    STEAM("steam"),
    EPIC("epic"),
    RIOT("riot");
}